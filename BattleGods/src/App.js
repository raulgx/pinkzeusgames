import React, { Component } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux';
import Gui from './Gui';
import CombatLog from './CombatLog';
const mapStateToProps = state => ({
  // viewStartX: state.viewStartX, viewStartY: state.viewStartY,
  // viewEndX: state.viewEndX, viewEndY: state.viewEndY,
  // maxX: state.maxX, maxY: state.maxY,
  // board: state.board,
  // entities: state.entities,
  // player: state.player
  ...state
});

const mapDispatchToProps = dispatch => ({
  MOVE: (location) =>
    dispatch({ type: 'MOVE', location }),
  HIT: (monsterLocation, playerHp, monsterHp) =>
    dispatch({ type: 'HIT', monsterLocation, playerHp, monsterHp}),
  REMOVE_ENTITY: (monsterLocation) =>
    dispatch({ type: 'REMOVE_ENTITY', monsterLocation}),
  LOSE: () =>
    dispatch({ type: 'LOSE' }),
  RESTART: () =>
    dispatch({ type: 'RESTART' }),
  WIN: () =>
    dispatch({ type: 'WIN' }),
  LOG: (message) =>
    dispatch({ type: 'ADD_LOG_MESSAGE', message }),
  SAVE: () =>
    dispatch({ type: 'SAVE' }),
  LOAD_SAVEGAMES: (loadGames) =>
    dispatch({ type: 'LOAD_SAVEGAMES', loadGames }),
  LOAD: (saveGame) =>
    dispatch({ type: 'LOAD', saveGame }),
  ADD_POINTS: (points) =>
    dispatch({ type: 'ADD_POINTS', points}),
  ADD_HEALTH: (location) =>
    dispatch({ type: 'ADD_HEALTH', location })
})

class App extends Component {
  componentWillMount() {
    this.props.MOVE({x: 17, y: 13});
  }

  lose() {
    setTimeout(this.props.RESTART, 5000);
    this.sendScore(this.props.points, this.props.timerStart);
    this.props.LOSE();
  }

  win() {
    setTimeout(this.props.RESTART, 5000);

    this.props.ADD_POINTS(1000);
    this.sendScore(this.props.points, this.props.timerStart);
    this.props.WIN();
  }

  sendScore(points, startTimeDate) {
    let currentTime = new Date();
    if (typeof startTimeDate === 'string')
      startTimeDate = new Date(startTimeDate)

    let secondsPassed = Math.floor((currentTime.getTime() - startTimeDate.getTime())/1000);
    let bonusMultiplier = 1;

    if (secondsPassed < 60)
      bonusMultiplier = 1.5;
    else if (secondsPassed < 300)
      bonusMultiplier = 1.3;
    else if (secondsPassed < 600)
      bonusMultiplier = 1.2;

    points *= bonusMultiplier;

    let dataPoints = {
      points: points,
      timeInSeconds: secondsPassed
    };

    fetch('/Points', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
		credentials: 'include',
        body: JSON.stringify(dataPoints)
      })
        .then(function (data) {
          console.log('Request success: ', data);
        })
        .catch(error => console.log(error))
  }

  fight(monsterLocation) {
    let player = this.props.player;
    let monster = this.props.entities[monsterLocation];

    let monsterHpAfterHit = monster.hp - player.attack;
    this.props.LOG(`You hit ${monster.name} for ${monster.hp - monsterHpAfterHit} hitpoints.`);
    if (monsterHpAfterHit <= 0) {
      this.props.HIT(monsterLocation, player.hp, 0);
      this.props.LOG('killed ' + monster.name);
      this.props.REMOVE_ENTITY(monsterLocation);
      this.props.ADD_POINTS(monster.points || 0);
      let [x, y] = monsterLocation.split('x');
      this.props.MOVE({ x: +x, y: +y }); //convert to numbers
    }
    else {
      let playerHpAfterHit = player.hp - monster.attack;
      if (playerHpAfterHit <= 0) {
        this.lose();
        return;
      }
      this.props.HIT(monsterLocation, playerHpAfterHit, monsterHpAfterHit);
    }

  }

  handleMove(input) {
    let { entities, maxX, maxY, healthPacks } = this.props;
    let player = this.props.player;
    let newPlayerX = player.x + input.x;
    let newPlayerY = player.y + input.y;

    if (this.props.winLocation === `${newPlayerX}x${newPlayerY}`)
      this.win();

    if (newPlayerX >= 0 && newPlayerY >= 0 &&  this.props.board[newPlayerY * maxX + newPlayerX] && newPlayerX < maxX && newPlayerY < maxY) {
        let monsterLocation = `${newPlayerX}x${newPlayerY}`;
        if (entities[monsterLocation])
          this.fight(monsterLocation);
        else if (healthPacks[monsterLocation]) {
          this.props.ADD_HEALTH(monsterLocation);
          this.props.MOVE({x: newPlayerX, y: newPlayerY});
        }
        else
          this.props.MOVE({x: newPlayerX, y: newPlayerY});
    }
  }

  handleKeypress(event) {
    let input;
    switch (event.key) {
      case 'w':
      case 'ArrowUp':
        input = {x:0, y:-1};
        break;
      case 's':
      case 'ArrowDown':
        input = {x:0, y:1};
        break;
      case 'a':
      case 'ArrowLeft':
        input = {x:-1, y:0};
        break;
      case 'd':
      case 'ArrowRight':
        input = {x:1, y:0};
        break;
      default:
        break;
    }
    if (input) {
      event.preventDefault();
      this.handleMove(input);
    }
  }
  componentDidMount() {
    window.addEventListener('keydown', this.handleKeypress.bind(this));
  }
  componentWillUnmount() {
    window.removeEventListener('keydown', this.handleKeypress);
  }
  render() {
    if (this.props.lost){
      return (
        <div id='board'>
          <h1>DEFEAT</h1>
        </div>
      );
    }

    if (this.props.won) {
      return (
        <div id='board'>
          <h1>WIN</h1>
        </div>
      );
    }

    const { board, healthPacks } = this.props;
    let { viewStartX, viewStartY, viewEndX, viewEndY, entities, maxX, maxY, winLocation } = this.props;
    let player = this.props.player;

    let rows = [], row, pixelClass;
    for (let y = viewStartY; y < viewEndY; y++) {
      row = [];

      for (let x = viewStartX; x < viewEndX; x++) {
        if (x < 0 || y < 0 || x >= maxX || y >= maxY)
          pixelClass = 'OUTERWALL'
        else if (x === player.x && y === player.y)
          pixelClass = 'PLAYER';
        else if (entities[`${x}x${y}`])
          pixelClass = 'ENEMY';
        else if (winLocation === `${x}x${y}`)
          pixelClass = 'WIN';
        else if (healthPacks[`${x}x${y}`])
          pixelClass = 'HEALTH';
        else if (board[y * maxX + x]) //1 is floor 0 is wall
          pixelClass = 'FLOOR';
        else
          pixelClass = 'WALL';

        row.push(<span className={"pixel " + pixelClass} key={x + "x" + y}></span>);
      }//for x
      rows.push(
        <div className="boardRow" key={"row" + y}>
          {row}
        </div>
      );
    }//for y

    return (
      <div>
        <div id='board'>
          {rows}
        </div>
        <Gui player={this.props.player} save={this.props.SAVE} points={this.props.points} load={this.props.LOAD} loadSaveGames={this.props.LOAD_SAVEGAMES} loadGames={this.props.loadGames}/>
        <CombatLog messages={this.props.combatLog} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
