import board from './map';
let maxX = 100,
  maxY = 100,
  loadGames = [];
let size = maxX * maxY,
  xSize = 49,
  ySize = 29;
let halfxRender = Math.floor(xSize / 2),
  halfyRender = Math.floor(ySize / 2);
let viewStartX = -halfxRender,
  viewStartY = -halfyRender,
  viewEndX = halfxRender,
  viewEndY = halfyRender;


const defaultState = {
  viewStartX, viewStartY, viewEndX, viewEndY, xSize, ySize,
  maxX, maxY,
  board, loadGames,
  entities: {
    '20x17': {
      name: 'Chimera',
      hp: 100,
      attack: 20,
      points: 100
    },
    '17x34': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '8x87': {
      name: 'Chimera',
      hp: 100,
      attack: 20,
      points: 100
    },
    '31x56': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '53x54': {
      name: 'Chimera',
      hp: 100,
      attack: 20,
      points: 100
    },
    '60x86': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '77x57': {
      name: 'Chimera',
      hp: 100,
      attack: 20,
      points: 100
    },
    '86x43': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '76x22': {
      name: 'Chimera',
      hp: 100,
      attack: 20,
      points: 100
    },
    '54x20': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '31x28': {
      name: 'Elite Chimera',
      hp: 100,
      attack: 30,
      points: 100
    },
    '90x73': {
      name: 'The Minotaur',
      hp: 200,
      attack: 50,
      points: 500
    }
  },
  player: {
    x: 17,
    y: 13,
    hp: 500,
    attack: 40
  },
  healthPacks: {
    '12x23': 50,
    '6x37': 50,
    '21x23': 50,
    '40x55': 50,
    '62x14': 50,
    '94x51': 50,
  },
  winLocation: '95x78',
  combatLog: [],
  timerStart: new Date(),
  points: 0
}

export default function appReducer(state = defaultState, action) {
  switch (action.type) {
    case 'MOVE':
      let halfxRender = Math.floor(state.xSize / 2),
        halfyRender = Math.floor(state.ySize / 2);
      return {
        ...state,
        player: {
          ...state.player,
          x: action.location.x,
          y: action.location.y
        },
        viewStartX: action.location.x - halfxRender,
        viewStartY: action.location.y - halfyRender,
        viewEndX: action.location.x + halfxRender,
        viewEndY: action.location.y + halfyRender
      }

    case 'HIT':
      return {
        ...state,
        player: {
          ...state.player,
          hp: action.playerHp,
        },
        entities: {
          ...state.entities,
          [action.monsterLocation]: {
            ...state.entities[action.monsterLocation],
            hp: action.monsterHp
          }
        }
      }

    case 'REMOVE_ENTITY':
      return {
        ...state,
        entities: {
          ...state.entities,
          [action.monsterLocation]: null
        }
      }

    case 'LOSE':
      return {
        lost: true
      }

    case 'WIN':
      return {
        won: true
      }

    case 'RESTART':
      return {
        ...defaultState,
        timerStart: new Date()
      }

    case 'ADD_LOG_MESSAGE':
      return {
        ...state,
        combatLog: [...state.combatLog,
        action.message
        ]
      }

    case 'ADD_POINTS':
      return {
          ...state,
          points: state.points + action.points
      }

    case 'SAVE':
      fetch('/saveGame', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(state)
      })
        .then(function (data) {
          console.log('Request success: ', data);
        })
        .catch(error => console.log(error))
      return {
        ...state
      }
    case 'LOAD_SAVEGAMES':
      return {
        ...state,
        loadGames: [...action.loadGames]
      }
    case 'LOAD':
      return {
        ...action.saveGame
      }
    case 'ADD_HEALTH':
      return {
        ...state,
        player: { ...state.player,
          hp: state.player.hp + state.healthPacks[action.location]
        },
        healthPacks: {...state.healthPacks,
          [action.location]: undefined
        }
      }
    default:
      return state;
  }
}
