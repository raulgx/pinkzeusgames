import React, { Component } from 'react';
import './App.css';

class CombatLog extends Component {
  render() {
    let rows = this.props.messages.map((value, index) => {
      return (
        <p key={index}>[CombatLog]:{value}</p>
      );
    });
    return (
      <div id='combat-log'>
        {rows}
      </div>
    )
  }

}

export default CombatLog;
