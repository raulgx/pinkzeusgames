import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import './App.css';

class Gui extends Component {
  constructor(props) {
    super(props)
    this.state = { showModal: false };
  }

  close() {
    this.setState({ showModal: false });
  }

  open() {
    this.setState({ showModal: true });
    var loadSaveGames = this.props.loadSaveGames;
    console.log(this.props)
    fetch('/saveGame', {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
	  credentials: 'include'
    }).then(item => item.json())
      .then(function (data) {
        console.log(data)
        loadSaveGames(data);
      })
      .catch(error => console.log(error))
  }
  load(i) {
    this.props.load(this.props.loadGames[i].saveGame);
  }
  render() {
    let rows = [];
    let {loadGames} = this.props;
    for (let i = 0; i < loadGames.length; i++) {
      let row = loadGames[i];
      rows.push(<p key={i} onClick={this.load.bind(this, i)}>{row.saveGame.timerStart} - LOAD</p>)
    }
    return (
      <div id='gui'>
        <h1>HP: {this.props.player.hp}</h1>
        <h1>Attack: {this.props.player.attack}</h1>
        <h1>Points: {this.props.points}</h1>
        <Button onClick={this.props.save}>SAVE</Button>
        <Button onClick={this.open.bind(this)}>LOAD</Button>
        <Modal show={this.state.showModal} onHide={this.close.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Loading savegames</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {rows}
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.close.bind(this)}>Close</Button>
          </Modal.Footer>
        </Modal>

      </div>
    )
  }

}

export default Gui;
