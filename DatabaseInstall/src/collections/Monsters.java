package collections;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class Monsters implements IDbCollection{
	public static String collectionName = "monsters";

	@Override
	public void addCollection(DB db) {
		if(!db.collectionExists(collectionName))
		{
			DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
			System.out.println("Collection created " + collectionName);
			addObjects(myCollection);
		}
		else {
			System.out.println("Collection exists " + collectionName);
		}	
	}

	@Override
	public void addObjects(DBCollection collection) {
		// TODO Auto-generated method stub
		Map<String, Object> m1 = new HashMap<String, Object>();
		m1.put("name", "m1");
		m1.put("LifePoints", "100");

		Map<String, Object> m2 = new HashMap<String, Object>();
		m2.put("name", "m2");
		m2.put("LifePoints", "200");

		collection.insert(new BasicDBObject(m1));
		collection.insert(new BasicDBObject(m2));

	}
	
}
