package collections;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class Leaderboards implements IDbCollection {
	public static String collectionName = "leaderboards";

	@Override
	public void addCollection(DB db) {
		if(!db.collectionExists(collectionName))
		{
			DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
			System.out.println("Collection created " + collectionName);
			addObjects(myCollection);
		}
		else {
			System.out.println("Collection exists " + collectionName);
		}	
	}

	@Override
	public void addObjects(DBCollection collection) {
		// TODO Auto-generated method stub
		Map<String, Object> player1 = new HashMap<String, Object>();
		player1.put("name", "player1");
		player1.put("points", "100");

		Map<String, Object> player2 = new HashMap<String, Object>();
		player2.put("name", "player2");
		player2.put("points", "200");

		collection.insert(new BasicDBObject(player1));
		collection.insert(new BasicDBObject(player2));

	}
	
}
