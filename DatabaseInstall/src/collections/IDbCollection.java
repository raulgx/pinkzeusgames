package collections;

import com.mongodb.DB;
import com.mongodb.DBCollection;

public interface IDbCollection {
	static String collectionName = "";
	void addCollection(DB db);
	void addObjects(DBCollection collection);
}
