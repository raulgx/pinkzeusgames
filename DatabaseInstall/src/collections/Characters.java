package collections;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class Characters implements IDbCollection{
	public static String collectionName = "characters";

	@Override
	public void addCollection(DB db) {
		if(!db.collectionExists(collectionName))
		{
			DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
			System.out.println("Collection created " + collectionName);
			addObjects(myCollection);
		}
		else {
			System.out.println("Collection exists " + collectionName);
		}	
	}

	@Override
	public void addObjects(DBCollection collection) {
		// TODO Auto-generated method stub
		Map<String, Object> character1 = new HashMap<String, Object>();
		character1.put("name", "character1");
		character1.put("LifePoints", "100");

		Map<String, Object> character2 = new HashMap<String, Object>();
		character2.put("name", "character2");
		character2.put("LifePoints", "200");

		collection.insert(new BasicDBObject(character1));
		collection.insert(new BasicDBObject(character2));

	}
	
}
