package collections;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class Items implements IDbCollection{
	public static String collectionName = "items";

	@Override
	public void addCollection(DB db) {
		if(!db.collectionExists(collectionName))
		{
			DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
			System.out.println("Collection created " + collectionName);
			addObjects(myCollection);
		}
		else {
			System.out.println("Collection exists " + collectionName);
		}	
	}

	@Override
	public void addObjects(DBCollection collection) {
		// TODO Auto-generated method stub
		Map<String, Object> item1 = new HashMap<String, Object>();
		item1.put("name", "item1");
		item1.put("LifePoints", "100");

		Map<String, Object> item2 = new HashMap<String, Object>();
		item2.put("name", "item2");
		item2.put("LifePoints", "200");

		collection.insert(new BasicDBObject(item1));
		collection.insert(new BasicDBObject(item2));

	}
	
}
