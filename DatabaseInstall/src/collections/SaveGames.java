package collections;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class SaveGames implements IDbCollection {
		public static String collectionName = "saveGames";

		@Override
		public void addCollection(DB db) {
			if(!db.collectionExists(collectionName))
			{
				DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
				System.out.println("Collection created " + collectionName);
				addObjects(myCollection);
			}
			else {
				System.out.println("Collection exists " + collectionName);
			}	
		}

		@Override
		public void addObjects(DBCollection collection) {

		}
		
}

