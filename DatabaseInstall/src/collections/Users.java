package collections;

import java.util.HashMap;
import java.util.Map;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class Users implements IDbCollection {
	public static String collectionName = "users";
	
	@Override
	public void addCollection(DB db) {
		if(!db.collectionExists(collectionName))
		{
			DBCollection myCollection = db.createCollection(collectionName, new BasicDBObject());      
			System.out.println("Collection created " + collectionName);
			addObjects(myCollection);
		}
		else {
			System.out.println("Collection exists " + collectionName);
		}
	}

	@Override
	public void addObjects(DBCollection collection) {
		Map<String, Object> user1 = new HashMap<String, Object>();
		user1.put("name", "cosmin");
		user1.put("points","1000");
		
		Map<String, Object> user2 = new HashMap<String, Object>();
		user2.put("name", "stefan");
		
		Map<String, Object> user3 = new HashMap<String, Object>();
		user3.put("name", "raul");

		collection.insert(new BasicDBObject(user1));
		collection.insert(new BasicDBObject(user2));
		collection.insert(new BasicDBObject(user3));
	}

}
