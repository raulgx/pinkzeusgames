package program;

import java.util.LinkedList;

import com.mongodb.DB;
import com.mongodb.MongoClient;

import collections.*;

public class Program {

	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );	
		
		@SuppressWarnings("deprecation")
		DB db = mongoClient.getDB("pinkzeusdb");
		
		//adds collections to the list to be added to db
		LinkedList<IDbCollection> collectionList = new LinkedList<IDbCollection>();
		collectionList.add(new Leaderboards());
		collectionList.add(new Users());
		collectionList.add(new Monsters());
		collectionList.add(new Items());
		collectionList.add(new Characters());
		collectionList.add(new SaveGames());
		
		for (IDbCollection collection : collectionList) {
			collection.addCollection(db);
		}
		
		mongoClient.close();
	}

}
