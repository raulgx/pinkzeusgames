package servicii.web;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.util.JSON;

import database.DBManager;

@Path("/leaderboards")
public class Leaderboards {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getLeaderboards() {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getLeaderboardsCollection();
		
		MongoCursor<Document> cursor = coll.find(new Document())
				.sort(new Document("points", -1)).limit(20)
				.iterator();
		
		BasicDBList list = new BasicDBList();
		
		try {
			while(cursor.hasNext()){
				Document item = cursor.next();
				list.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		
		return JSON.serialize(list);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postLeaderboards(String reqJson) {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getLeaderboardsCollection();
		
		Document insertObject = Document.parse(reqJson);
		coll.insertOne(insertObject);

		System.out.println("posted score");
		return Response.ok().build();
	}
}
