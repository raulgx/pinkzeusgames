package servicii.web;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import database.DBManager;

@Path("/Login")
public class Login {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postUsers(String reqJson) {//register
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getUsersCollection();
		
		Document insertObject = Document.parse(reqJson);
		String user = (String) insertObject.get("name");
		
		MongoCursor<Document> cursor = coll.find(new Document("name", user)).iterator();
		
		if (!cursor.hasNext())
			return Response.status(401).build();
		
		try {
			Document databesItem = cursor.next();
			String password = databesItem.getString("password");
			if (!password.equals((String) insertObject.get("password"))) {
				return Response.status(401).build();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		

		return Response.ok().build();
	}
}
