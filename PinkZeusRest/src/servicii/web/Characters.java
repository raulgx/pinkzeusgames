package servicii.web;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.util.JSON;

import database.DBManager;

@Path("/characters")
public class Characters {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getCharacters() {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getCharactersCollection();
		
		MongoCursor<Document> cursor = coll.find(new Document()).iterator();
		BasicDBList list = new BasicDBList();
		
		try {
			while(cursor.hasNext()){
				Document item = cursor.next();
				list.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		
		return JSON.serialize(list);
	}
}
