package servicii.web;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import database.DBManager;

@Path("/users")
public class Users {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getUsers() {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getUsersCollection();
		
		MongoCursor<Document> cursor = coll.find(new Document()).iterator();
		StringBuilder response = new StringBuilder();
		
		try {
			while(cursor.hasNext()){
				Document item = cursor.next();
				response.append(item.get("name") + "<br>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		
		return response.toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postUsers(String reqJson) {//register
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getUsersCollection();
		
		Document insertObject = Document.parse(reqJson);
		String user = (String) insertObject.get("name");
		
		MongoCursor<Document> cursor = coll.find(new Document("name", user)).iterator();
		
		if (cursor.hasNext())
			return Response.status(401).build();
		
		coll.insertOne(insertObject);

		return Response.ok().build();
	}
}
