package servicii.web;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.bson.Document;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;

import database.DBManager;

@Path("/items")
public class Items {
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getItems() {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getItemsCollection();
		
		MongoCursor<Document> cursor = coll.find(new Document()).iterator();
		StringBuilder response = new StringBuilder();
		
		try {
			while(cursor.hasNext()){
				Document item = cursor.next();
				response.append(item.get("name") + "<br>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		
		return response.toString();
	}
}
