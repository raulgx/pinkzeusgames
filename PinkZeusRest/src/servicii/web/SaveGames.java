package servicii.web;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.bson.Document;

import com.mongodb.BasicDBList;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.util.JSON;

import database.DBManager;

@Path("/saveGames")
public class SaveGames {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{user}")
	public String getSaveGames(@PathParam("user")String user) {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getSaveGameCollection();
		MongoCursor<Document> cursor = coll.find(new Document("name", user)).iterator();
		BasicDBList list = new BasicDBList();
		
		try {
			while(cursor.hasNext()){
				Document item = cursor.next();
				list.add(item);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			cursor.close();
		}
		
		return JSON.serialize(list);
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response postSaveGames(String reqJson) {
		DBManager dbManager = DBManager.getInstance();
		MongoCollection<Document> coll = dbManager.getSaveGameCollection();
		
		Document insertObject = Document.parse(reqJson);

		coll.insertOne(insertObject);
		
		System.out.println("posted savegame");
		return Response.ok().build();
	}
}
