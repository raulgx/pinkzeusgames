package database;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class DBManager {
	private static DBManager instance = null;
	private MongoCollection<Document> usersCollection;
	private MongoCollection<Document> leaderboardsCollection;
	private MongoCollection<Document> monstersCollection;
	private MongoCollection<Document> itemsCollection;
	private MongoCollection<Document> charactersCollection;
	private MongoCollection<Document> saveGamesCollection;

	protected DBManager() {
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		MongoDatabase db = mongoClient.getDatabase("pinkzeusdb");
		usersCollection = db.getCollection("users");
		leaderboardsCollection = db.getCollection("leaderboards");
		monstersCollection = db.getCollection("monsters");
		itemsCollection = db.getCollection("items");
		charactersCollection = db.getCollection("characters");
		saveGamesCollection = db.getCollection("saveGames");
	}
	
	public static DBManager getInstance() {
		if (instance == null) {
			synchronized(DBManager.class) {
				if (instance == null) {
					instance = new DBManager();
				}
			}
		}
		return instance;
	}

	public MongoCollection<Document> getUsersCollection() {
		return usersCollection;
	}

	public MongoCollection<Document> getLeaderboardsCollection() {
		return leaderboardsCollection;
	}
	
	public MongoCollection<Document> getMonstersCollection() {
		return monstersCollection;
	}
	
	public MongoCollection<Document> getItemsCollection() {
		return itemsCollection;
	}
	
	public MongoCollection<Document> getCharactersCollection() {
		return charactersCollection;
	}
	
	
	public MongoCollection<Document> getSaveGameCollection() {
		return saveGamesCollection;
	}
}
