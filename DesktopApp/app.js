var http = require('http');
exports.getLeaderboards = function (doc) {
  var options = {
    host: 'localhost',
    path: '/rest/leaderboards',
    port: 8989
  };
  var body;
  var req = http.get(options, function (res) {
    console.log('STATUS: ' + res.statusCode);
    console.log('HEADERS: ' + JSON.stringify(res.headers));

    // Buffer the body entirely for processing as a whole.
    var bodyChunks = [];
    res.on('data', function (chunk) {
      // You can process streamed parts here...
      bodyChunks.push(chunk);
    }).on('end', function () {
      doc.open()
      body = Buffer.concat(bodyChunks);
      var responseJson = JSON.parse(body);
      doc.write('<button onclick="request.getLeaderboards(document)">REFRESH</button>')
      responseJson.forEach(function(element) {
        doc.write("<p>")
        doc.write("Name: " + element.username + " points: " + element.points + " time: " + element.timeInSeconds)
        doc.write("</p>")
      });
    })
  });

}
