package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import resthelper.Constants;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
	          
	    String n=request.getParameter("username");  
	    String p=request.getParameter("userpass");  
	          	          
	    Gson gson = new Gson(); 
	    
	    Map<String, String> myMap = new HashMap<String, String>(); ;
		myMap.put("name", n);
		myMap.put("password", p);
	    
	    ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
	    
	    WebTarget target = client.target(Constants.getBaseURI());
		Response responseFromRest = target.path("rest").path("Login").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(gson.toJson(myMap)));
		
		if(!Response.Status.Family.familyOf(responseFromRest.getStatus()).equals(Response.Status.Family.SUCCESSFUL)){
		    response.sendRedirect("login.jsp");
		} 
		else
		{
			HttpSession session = request.getSession();
			System.out.println(n);
			session.setAttribute("user", n);
			//setting session to expiry in 30 mins
			session.setMaxInactiveInterval(30*60);
			Cookie userName = new Cookie("user", n);
			userName.setMaxAge(30*60);
			response.addCookie(userName);
			response.sendRedirect("index.jsp");
			
		}
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
