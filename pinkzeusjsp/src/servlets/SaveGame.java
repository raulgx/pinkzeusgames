package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import resthelper.Constants;

/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/saveGame")
public class SaveGame extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveGame() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = "gheruta";
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(Constants.getBaseURI());
		String responseFromRest = target.path("rest").path("saveGames/" + user).request(MediaType.APPLICATION_JSON)
				.get(String.class);
		//response.setContentType("application/json");
		// Get the printwriter object from response to write the required json object to the output stream      
		PrintWriter out = response.getWriter();
		// Assuming your json object is **jsonObject**, perform the following, it will return your json object  
		out.write(responseFromRest);
		out.flush();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StringBuffer jb = new StringBuffer();
		String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { /*report an error*/ }
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		String sendJson = jb.toString();
		Gson gson = new Gson();
		Object sendObj = gson.fromJson(sendJson, new TypeToken<Map<String, Object>>(){}.getType());
		Map sendMap = new HashMap<String, Object>();
		
		HttpSession session = request.getSession(true);
		String username = (String)session.getAttribute("user");
		System.out.println("user "+ username + " saved");
		sendMap.put("name", username);
		sendMap.put("saveGame", sendObj);
		WebTarget target = client.target(Constants.getBaseURI());
		Response responseFromRest = target.path("rest").path("saveGames").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(jb.toString()));
		
		if(!Response.Status.Family.familyOf(responseFromRest.getStatus()).equals(Response.Status.Family.SUCCESSFUL)){
		    response.sendError(400);
		}
	}
	
}