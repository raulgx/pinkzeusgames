package servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import com.google.gson.Gson;
import resthelper.Constants;

/**
 * Servlet implementation class Register
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Register() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
	          
	    String n=request.getParameter("name");  
	    String p=request.getParameter("password");  
	          	          
	    Gson gson = new Gson(); 
	    
	    Map<String, String> myMap = new HashMap<String, String>(); ;
		myMap.put("name", n);
		myMap.put("password", p);
	    
	    ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);
	    
	    WebTarget target = client.target(Constants.getBaseURI());
		Response responseFromRest = target.path("rest").path("Register").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(gson.toJson(myMap)));
		
		if(!Response.Status.Family.familyOf(responseFromRest.getStatus()).equals(Response.Status.Family.SUCCESSFUL)){
		    response.sendRedirect("register.jsp");
		} 
		else
		{
			response.sendRedirect("login.jsp");	
		}
	    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
