package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import resthelper.Constants;

/**
 * Servlet implementation class Points
 */
@WebServlet("/Points")
public class Points extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Points() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		{  post json should look like this
//		    "points": 100,
//		    "timeInSeconds": 30
//		}
		
		StringBuffer jb = new StringBuffer();
		String line = null;
		  try {
		    BufferedReader reader = request.getReader();
		    while ((line = reader.readLine()) != null)
		      jb.append(line);
		  } catch (Exception e) { /*report an error*/ }
		
	  	HttpSession session = request.getSession(true);
		String user =(String)session.getAttribute("user");
		
		Gson gson = new Gson(); 
		Map<String, String> myMap = gson.fromJson(jb.toString(), new TypeToken<Map<String, Object>>(){}.getType());
		myMap.put("username", user); //COSMIN aici pui usernameul
		
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(Constants.getBaseURI());
		Response responseFromRest = target.path("rest").path("leaderboards").request(MediaType.APPLICATION_JSON)
				.post(Entity.json(gson.toJson(myMap)));
		
		if(!Response.Status.Family.familyOf(responseFromRest.getStatus()).equals(Response.Status.Family.SUCCESSFUL)){
		    response.sendError(400);
		}
	}
	
}
