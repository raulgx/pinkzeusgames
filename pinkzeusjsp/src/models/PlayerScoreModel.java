package models;

public class PlayerScoreModel {
	public double points;
	public String username;
	public double timeInSeconds;
	
	public PlayerScoreModel(double points, String name, double timeInSeconds) {
		this.points = points;
		this.username = name;
		this.timeInSeconds = timeInSeconds;
	}
}
