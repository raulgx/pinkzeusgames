package resthelper;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

public class Constants {
	public static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8989").build();
	}
}
