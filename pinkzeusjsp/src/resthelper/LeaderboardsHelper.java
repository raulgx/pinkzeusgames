package resthelper;
import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;

import com.google.gson.Gson;

import models.PlayerScoreModel;


public class LeaderboardsHelper {
	public static PlayerScoreModel[] getLeaderboards() {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(getBaseURI());

		String jsonResponse = target.path("rest").path("leaderboards").request().accept(MediaType.APPLICATION_JSON)
				.get(String.class);
		
		Gson gson = new Gson();
	    return gson.fromJson(jsonResponse, PlayerScoreModel[].class);
	}
	
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8989").build();
	}
}
