package resthelper;
import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;


public class UserHelper {
	public static String getUsers() {
		ClientConfig config = new ClientConfig();
		Client client = ClientBuilder.newClient(config);

		WebTarget target = client.target(getBaseURI());

		return target.path("rest").path("users").request().accept(MediaType.TEXT_PLAIN)
				.get(String.class);
	}
	
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8989").build();
	}
}
