<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
  <title>Battle GODS by Pink Zeus Games</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/index_style.css">

<nav class="navbar <!-- navbar-fixed-top--> main-nav-outer" role="navigation">
	  	<div class="navbar-header">
			<a class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		       <i class="fa fa-bars fa-2x text-center" aria-hidden="true"></i>
			</a>
		</div>
		<img class="navbarHomeLogo navbar-brand" src="../images/pink zeus.png">
		<div class="navbar-collapse collapse">
			<ul class="main-nav"> 
	        	<li><a href="index.jsp">Home</a></li>
	            <li><a href="game.jsp">The Game</a></li>
	            <li><a href="users.jsp">Users list</a></li>
	            <!-- <li><a href="characters.jsp">Gods</a></li> -->
	            <li><a href="monsters.jsp">Monsters</a></li>
	           	<li><a href="items.jsp">Items</a></li>
	           	<li><a href="leaderboards.jsp">Leaderboards</a></li>
<%
	Object username =  request.getSession().getAttribute("user");	
if( username == null) {
	%> <li><a href="login.jsp"><i class="fa fa-sign-in" aria-hidden="true"></i>  Sign In</a></li>
	<%
}
else{
	%> <li><a href="Logout"><i class="fa fa-sign-in" aria-hidden="true"></i>  Logout</a></li>
	<%
}
    %>
	 			
	        </ul>
	    </div>
</nav>

</head>
<body class="body_all">
 
 
<div class="container-fluid text-center">    

  <div class="col-sm-2 sidenav">
    </div>
    <div class="haha1">
    	<hr class="new-hr-style">
  			<h1>Pink Zeus Studios Official Page</h1>
  		<hr class="new-hr-style">
  	</div>
  	
  <div class="row content">
  	<div class="col-sm-8"> 
      
      	<iframe src="https://drive.google.com/file/d/0B2NORL8W9z6ndFZ0b1NSdmZ1dk0/" style="width:718px; height:700px;" frameborder="0"></iframe>
      
      
    </div>
    
    <div class="col-sm-2 sidenav">
    </div>
  </div>
</div>

<footer>
	<div id="footerwrap">
	 	<div class="container">
		 	<div class="row">
		 		<div class="col-lg-4">
		 			<h4>IT Group</h4>
						<div class="hline-w"></div>
						<ul class="list-group">		
								<li class="list-group-item"><a href="#Home" onclick="schimba('home')">Home</a></li>
								<li class="list-group-item"><a href="#About">About</a></li>
								<li class="list-group-item"><a href="#" onclick="schimba('projects')">Projects</a></li>
								<li class="list-group-item"><a href="#Proposals">Proposals</a></li>
								<li class="list-group-item"><a href="#Contact">Contact</a></li>
						</ul>

		        <br>
				
					<h4>Site AC TUIASI</h4>
						<div class="hline-w"></div>
						<a href="http://www.ac.tuiasi.ro" target="__blank"><img class="logo-footer-ac" src="images/logo_ac.png" alt="Logo AC TUIASI"></a>
		 		</div>
		 		<div class="col-lg-4">
		 			<h4>Contact</h4>
						<div class="hline-w"></div>
						<form class="form-horizontal" role="form" method="post" action="index.php">
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="email" class="col-sm-2 control-label">Email</label>
								<div class="col-sm-10">
									<input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
								</div>
							</div>
							<div class="form-group">
								<label for="message" class="col-sm-2 control-label">Message</label>
								<div class="col-sm-10">
									<textarea class="form-control" rows="4" name="message"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-10 col-sm-offset-2">
									<!-- Will be used to display an alert to the user-->
								</div>
							</div>
						</form>
				
					<br>

		 			<h4>Social Links</h4>
						<div class="hline-w"></div>
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-bitbucket"></i></a>
						<a href="#"><i class="fa fa-youtube"></i></a>
						<a href="#"><i class="fa fa-google"></i></a>
		 		</div>
		 		
		 		<div class="col-lg-4">
		 			<h4>Adress</h4>
						<div class="hline-w"></div>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2713.1982241696737!2d27.591783215085425!3d47.15397202712922!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40cafb9996b838b1%3A0xc8b2b5b97fb1b7d8!2sFaculty+of+Automatic+Control+and+Computer+Engineering!5e0!3m2!1sen!2sus!4v1480112205148" width="300" height="300" frameborder="0"></iframe>
						<div class="row">
							<img class="logo-footer-redim" src="../images/logo_navbar_white.png" alt="Logo Footer Bar">
							<p>&copy; IT Group 2016</p>
						</div>
		 		</div>
		 	</div>
	 	</div>
		
	 	<br>
		
	 </div>
</footer>

</body>
</html>