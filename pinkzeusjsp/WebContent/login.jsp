<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" session="true" %>
<%@ page import="servlets.Login" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Flat HTML5/CSS3 Login Form</title>
<link rel="stylesheet" href="css/login_style.css">
</head>
<body>
	<div class="login-page">
		<div class="form">
			<form class="login-form" action="Login" method="post">
				<input type="text" placeholder="username" name="username"/> 
				<input type="password" placeholder="password" name="userpass" />
				<button type="submit">login</button>
				<p class="message">
					Not registered? <a href="register.jsp">Create an account</a>
				</p>
			</form>
		</div>
	</div>
	<script
		src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script src="js/login.js"></script>
</body>
</html>